<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>The Complete Guide To Anxiety-Free Life</title>

        <!-- GA tag-->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163543014-2"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-602280382"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-163543014-2');
            gtag('config', 'AW-602280382');
        </script>

        @if(isset($_GET['leadSaved']))
        <!-- Event snippet for Website lead conversion page -->
        <script>
        gtag('event', 'conversion', {'send_to': 'AW-602280382/9tOSCODi2tsBEL6jmJ8C'});
        </script>
        @endif

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
        </style>
    </head>
    <body>
        <div class="logo">
            <img src="/images/logo.png" alt="HealthInsider"/>
        </div>
        <div class="content">
            <div class="container">
                @if(isset($_GET['leadSaved']))
                <div class="alert alert-success" role="alert">
                    Congratulations! Your Complete guide to Anxiety-Free life is on the way to your inbox!
                </div>
                @endif
                <h1>Struggling with anxiety, stress or depression?</h1>
                <p class="description">
                    Our team of health professionals prepared a complete and 
                    thorough guide for people struggling with stress, anxiety or depression, 
                    which gives you full understanding of your condition and concrete action plan
                    to get rid of it!
                </p>
                <div class="content-blocks">
                    <div class="block tl-r">
                        <img src="/images/cover.png" class="cover" alt="Cover"/>
                    </div>
                    <div class="block form-block">
                        <h2>Get your <strong>free copy</strong> of The Complete Guide To Anxiety-Free Life now</h2>
                        <form id="lead-form" action="/saveLead" method="POST" name="lead-form">
                            @csrf
                            <input type="hidden" name="source" value="anxiety"/>
                            <label for="name">Your name (optional):</label>
                            <input id="name" type="text" class="form-control" placeholder="Enter your name" name="name"/>
                            <label for="email">Your email address*:</label>
                            <input id="email" type="text" class="form-control" placeholder="Enter your email" name="email"/>
                            <button class="btn cta-btn" name="get-copy" type="submit">Get my free copy</button>
                            <small>We respect your privacy. We will never sell, rent or share your email address. That's more than a policy, it's our personal guarantee!</small>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        function validateEmail(email) {
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        $(document).ready(function() {
            $('#lead-form').submit(function() {
                let email = $('#email').val();
                let isValid = validateEmail(email);
                if(!isValid) {
                    alert("Please enter a valid email address to get your free copy!");
                }

                return isValid;
            });
        });
    </script>
</html>
