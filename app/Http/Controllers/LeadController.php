<?php

namespace App\Http\Controllers;

use App\Lead;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class LeadController extends Controller
{
    public function __construct()
    {
        
    }

    public function saveLead(Request $request) {
        if(!empty($request->email)) {
            $leadData = [
                'name' => $request->name,
                'email' => $request->email,
                'source' => $request->source
            ];

            Lead::create($leadData);
        }

        return redirect("anxiety?leadSaved=1");
    }
}
