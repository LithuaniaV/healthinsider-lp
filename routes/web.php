<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("https://healthinsider.news");
});

Route::get('/anxiety', function() {
    return view('anxiety');
});
Route::post('/saveLead', 'LeadController@saveLead')->name('save-lead');